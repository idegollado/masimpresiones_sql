CREATE TRIGGER `ordenes_flexo_after_update` AFTER UPDATE ON `ordenes_flexo`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);

END

CREATE TRIGGER `ordenes_grabado_after_update` AFTER UPDATE ON `ordenes_grabado`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);
END

CREATE TRIGGER `ordenes_impresiones_after_update` AFTER UPDATE ON `ordenes_impresiones`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);
END

CREATE TRIGGER `ordenes_lonas_after_update` AFTER UPDATE ON `ordenes_lonas`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);
END

CREATE TRIGGER `ordenes_serigrafia_after_update` AFTER UPDATE ON `ordenes_serigrafia`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);
END

CREATE TRIGGER `ordenes_sublimacion_after_update` AFTER UPDATE ON `ordenes_sublimacion`
 FOR EACH ROW BEGIN

   CALL changeStatusGeneral(NEW.noorden);
END