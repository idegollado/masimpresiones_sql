BEGIN

    DECLARE imp INT unsigned;
    DECLARE flexo INT unsigned;
    DECLARE grabado INT unsigned;
    DECLARE seri INT unsigned;
    DECLARE lonas INT unsigned;
    DECLARE subli INT unsigned;

    SELECT COALESCE((SELECT ordenes_impresiones.status FROM ordenes_impresiones WHERE ordenes_impresiones.noorden = id_orden),1) INTO imp;
    SELECT COALESCE((SELECT ordenes_flexo.status FROM ordenes_flexo WHERE ordenes_flexo.noorden = id_orden),1) AS Flexo INTO flexo;
    SELECT COALESCE((SELECT ordenes_grabado.status FROM ordenes_grabado WHERE ordenes_grabado.noorden = id_orden),1) INTO grabado;
    SELECT COALESCE((SELECT ordenes_lonas.status FROM ordenes_lonas WHERE ordenes_lonas.noorden = id_orden),1) INTO seri;
    SELECT COALESCE((SELECT ordenes_serigrafia.status FROM ordenes_serigrafia WHERE ordenes_serigrafia.noorden = id_orden),1) INTO lonas;
    SELECT COALESCE((SELECT ordenes_sublimacion.status FROM ordenes_sublimacion WHERE ordenes_sublimacion.noorden = id_orden),1) INTO subli;

    IF imp = 1 AND flexo = 1 AND grabado = 1 AND seri = 1 AND subli = 1 AND lonas = 1 THEN
        UPDATE ordenes SET ordenes.status = 1 WHERE ordenes.noorden = id_orden;
    END IF;

END
